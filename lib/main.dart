import 'dart:ffi';
import 'dart:math';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: MyHomePage(),
  ));
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<String> entries = <String>[
    'Player 1',
    'Player 2',
    'Player 3',
    'Player 4',
    'Player 5',
    'Player 6',
    'Player 7'
  ];
  final List<int> colorCodes = <int>[700, 500, 100, 200, 300, 400, 600];
  List<String> newEntry = [];
  List<String> avatars = [
    'assets/eren.jpg',
    'assets/luffy.jpg',
    'assets/preuzmi.jpg',
    'assets/reevs.jpg',
    'assets/penguin.jpg',
    'assets/squidward.jpg',
    'assets/levi.jpg'
  ];
  List<int> bodovi = [0, 0, 0, 0, 0, 0, 0];
  int numberOfPlayers = 0;
  int numberOfPoints = 0;
  bool buttonDisabler = false;
  int incrementIndex = 0;
  int round = 0;
  String king = 'King';
  String servant = 'Servant';

  bool _isButtonDisabled() {
    if (numberOfPlayers < entries.length) {
      return false;
    } else {
      return true;
    }
  }

  bool _isButtonDisabled2() {
    if (numberOfPlayers < 3) {
      return true;
    } else {
      return false;
    }
  }

  void incrementRound() {
    setState(() {
      round++;

      if (incrementIndex < numberOfPlayers) {
        newEntry.add(entries[incrementIndex]);
        var random = Random();
        var random2 = Random();
        int randomNumber2 = random2.nextInt(newEntry.length);
        int randomNumber = random.nextInt(newEntry.length);

        king = newEntry[randomNumber];
        servant = newEntry[randomNumber2];
        incrementIndex++;
      } else {
        var random = Random();
        var random2 = Random();
        int randomNumber2 = random2.nextInt(newEntry.length);
        int randomNumber = random.nextInt(newEntry.length);

        king = newEntry[randomNumber];
        servant = newEntry[randomNumber2];
      }
    });
  }

  void addPlayer() {
    setState(() {
      numberOfPlayers++;
    });
  }

  void resetGame() {
    setState(() {
      numberOfPlayers = 0;
      round = 0;
      incrementIndex = 0;
      king = 'King';
      servant = 'Servant';
      newEntry.clear();
      colorCodes.shuffle();
      entries.shuffle();
      avatars.shuffle();
      bodovi = [0, 0, 0, 0, 0, 0, 0];
    });
  }

  @override
  void initState() {
    colorCodes.shuffle();
    entries.shuffle();
    avatars.shuffle();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text(
          'Kings order',
          style: TextStyle(
            color: Colors.amber,
            letterSpacing: 2,
          ),
        ),
        backgroundColor: Colors.grey[850],
        centerTitle: true,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.autorenew),
          onPressed: () {
            resetGame();
          },
          color: Colors.amber[300],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _isButtonDisabled() ? null : addPlayer,
        child: Icon(
          Icons.add,
          color: Colors.amber[300],
        ),
        backgroundColor: Colors.grey[800],
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.whatshot_outlined,
                      color: Colors.amber[500],
                      size: 50,
                    ),
                    Text(
                      king,
                      style: TextStyle(
                        color: Colors.amber,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
                Spacer(),
                Row(
                  children: [
                    Text(
                      servant,
                      style: TextStyle(
                        color: Colors.amber,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      Icons.surround_sound,
                      color: Colors.amber[700],
                      size: 50,
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              child: Container(
                height: 400,
                child: ListView.separated(
                  shrinkWrap: true,
                  padding: const EdgeInsets.all(8),
                  itemCount: numberOfPlayers,
                  itemBuilder: (BuildContext context, int randomVar) {
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.amber[colorCodes[randomVar]],
                        boxShadow: [
                          BoxShadow(color: Colors.black, spreadRadius: 3),
                        ],
                      ),
                      height: 70,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          CircleAvatar(
                            backgroundImage: AssetImage(avatars[randomVar]),
                            radius: 30,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: 120,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${entries[randomVar]}',
                                        style: TextStyle(
                                          fontSize: 15,
                                          letterSpacing: 1,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        'Points: ' +
                                            bodovi[randomVar].toString(),
                                        style: TextStyle(
                                          fontSize: 15,
                                          letterSpacing: 1,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          IconButton(
                            icon: Icon(Icons.add),
                            onPressed: () {
                              setState(() {
                                bodovi[randomVar] += 1;
                              });
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.remove),
                            onPressed: () {
                              setState(() {
                                bodovi[randomVar] -= 1;
                              });
                            },
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                ),
              ),
            ),
            SizedBox(
              height: 13,
            ),
            Row(
              children: [
                RaisedButton(
                  onPressed: _isButtonDisabled2() ? null : incrementRound,
                  color: Colors.amber[600],
                  child: Text(
                    'Next round',
                    style: TextStyle(
                      color: Colors.grey[50],
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(14.0))),
                ),
                SizedBox(
                  width: 40,
                ),
                Text(
                  'Round: ' + round.toString(),
                  style: TextStyle(
                    color: Colors.amber,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
